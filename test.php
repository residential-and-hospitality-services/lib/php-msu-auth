<?php

declare(strict_types = 1);

require "msu-auth.php";

/**
 * Interactively obtain credentials for a given domain.
 * @param string $domain
 * @return array An array in the form [$username, $password].
 */
function auth_dialog(string $domain) : array
{
    echo "Authenticating against $domain\n";
    echo "Leave blank to use fake credentials.\n";

    echo "Username: ";
    $username = rtrim(fgets(STDIN), "\r\n");

    if (!strlen($username)) {
        $fakes = [
            "test" . sprintf("%04d", random_int(0, 9999)),
            bin2hex(random_bytes(5))
        ];
        echo "Will use credentials: ${fakes[0]} / ${fakes[1]}.\n";
        return $fakes;
    }

    system("stty -echo 2>/dev/null");
    echo "Password: ";
    $password = rtrim(fgets(STDIN), "\r\n");
    echo "\n";
    system("stty echo 2>/dev/null");

    return [$username, $password];
}

$config = parse_ini_file(dirname(__FILE__) . "/" . "msu-auth.ini");

if (msu_authenticate(...auth_dialog($config["domain"]))) {
    echo "Credentials accepted.\n";
} else {
    echo "Credentials rejected.\n";
}
