FROM php:7.3-cli-alpine3.8

RUN apk add openldap \
    && apk add --virtual .build-deps openldap-dev \
    && docker-php-ext-install ldap \
    && apk del .build-deps

WORKDIR /srv/app
COPY msu-auth.ini msu-auth.php test.php ./
RUN find . -type f -exec chmod 644 \{\} \;

USER nobody

CMD ["php", "test.php"]
