<?php

declare(strict_types = 1);

/**
 * Validate provided credentials against MSU's CampusAD domain by authenticating
 * with LDAP.
 *
 * @param string $username
 * @param string $password
 * @return bool Returns true if validated, false for any failure.
 */
function msu_authenticate(string $username, string $password): bool
{
    if (!strlen($username) || !strlen($password)) {
        return false;
    }

    $config = parse_ini_file(dirname(__FILE__) . "/" . "msu-auth.ini");
    $con = ldap_connect($config["ldap_uri"]);
    if (!ldap_start_tls($con)) {
        return false;
    }

    /*
     * Caution: ldap_bind will attempt an anonymous bind if either the RDN
     * (username) or password is an empty string: we must not permit either of
     * these parameters to be blank. (The check of $username above is not
     * necessary, since we prepend it with a configuration variable and '\\',
     * but it seems like good form.)
     */
    if (@ldap_bind($con, "{$config["domain"]}\\$username", $password)) {
        return true;
    }

    return false;
}
