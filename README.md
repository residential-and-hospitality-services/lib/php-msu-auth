# php-msu-auth

This provides a simple PHP function to authenticate users using Campus
credentials. It verifies by authenticating against the CampusAD LDAP service.

## Requirements

* PHP 7 with the LDAP module enabled
* OpenLDAP

You may need to adjust OpenLDAP to be able to verify CampusAD's TLS certificate.
On Enterprise Linux 7 and Fedora 27, OpenLDAP doesn't ship configured to load
any certificate authority certificates. To configure it to use the certificates
already installed on most systems, add this line to `/etc/openldap/ldap.conf`:

    TLS_CACERT /etc/pki/ca-trust/extracted/pem/tls-ca-bundle.pem

## Usage

Drop `msu-auth.php` and `msu-auth.ini` somewhere in your project tree and
include `msu-auth.php` however you prefer. Use it like so:

```php
if (msu_authenticate($username, $password)) {
    echo "You're authenticated.";
} else {
    http_response_code(403);
    exit;
}
```

## Demo

With Docker:

```sh
sudo docker build -t php-msu-auth .
sudo docker run --rm -it php-msu-auth
sudo docker image rm php-msu-auth
```

With Vagrant:

```sh
vagrant up
vagrant ssh -- -t php /vagrant/test.php
vagrant destroy -f
```
